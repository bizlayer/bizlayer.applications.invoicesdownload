﻿
#region Importacions

using System;
using System.Data.OleDb;
using System.Configuration;
using System.IO;
using System.ServiceModel;

#endregion Importacions

namespace Bizlayer.Applications.InvoicesDownload
{

	public static class Program
	{

		#region Events

		/// <summary>
		/// Controla el punt d'entrada de l'aplicació
		/// </summary>
		/// <param name="prmArguments">Un System.Array amb els arguments de la crida</param>
		static void Main(String[] prmArguments)
		{
			try
			{
				// Comprova que es passen les validacions
				if (Validate())
				{
					// Processa l'arxiu amb les dades
					Process();
				}
			}
			catch (Exception ectMain)
			{
				// Registra l'error per pantalla
				Console.WriteLine("ERROR: " + ectMain.Message);
			}
			// Registra la finalització de l'execució per pantalla
			Console.WriteLine("INFO: Process end");
			Console.ReadKey(true);
		}

		#endregion Events


		#region Mètodes

		#region Privats

		private static void Process()
		{
			// Crea els contenidors de accés a l'arxiu csv
			String sCsvFilePath = String.Empty;
			StreamReader smrCsvFile = null;
			String sCsvLine = String.Empty;
			// Crea el contenidor de la ruta d'emmagatzemament de registres
			String sLogFilePath = String.Empty;
			// Crea els contenidors de les dades de l'usuari
			String sUserName = String.Empty;
			String sUserPassword = String.Empty;
			// Crea els contenidors de les dades de la factura
			String sInvoiceRef = String.Empty;
			String sSenderFiscalId = String.Empty;
			// Crea el contenidor de la resposta del servei
			WebService.RespuestaWsDescargarFacturas itmResponse = null;
			// Crea els contenidors de creació de l'arxiu de factura
			String sInvoiceFilePath = String.Empty;
			FileStream fsmInvoice = null;
			Byte[] bFileData = null;
			try
			{
				// Connecta amb l'arxiu
				sCsvFilePath = ConfigurationManager.AppSettings["DtaFilePath"];
				smrCsvFile = new StreamReader(sCsvFilePath);
				// Comprova que es pot accedir a les dades
				if (smrCsvFile != null && smrCsvFile.BaseStream.CanRead)
				{
					// Recull la ruta d'emmagatzemament de registres
					sLogFilePath = ConfigurationManager.AppSettings["LogDirectoryPath"];
					// Recull les dades de l'usuari
					sUserName = ConfigurationManager.AppSettings["SvcUserName"];
					sUserPassword = ConfigurationManager.AppSettings["SvcUserPassword"];
					// Llegeix la capçalera de l'arxiu
					sCsvLine = smrCsvFile.ReadLine();
					// Processa les dades recollides
					while (!smrCsvFile.EndOfStream)
					{
						// Recull una línia de l'arxiu
						sCsvLine = smrCsvFile.ReadLine();
						// Recull l'identificador fiscal de l'emisor
						sSenderFiscalId = sCsvLine.Split(';')[0];
						// Recull la referència de la factura
						sInvoiceRef = sCsvLine.Split(';')[1];
						// Comprova que s'han pogut recollir les dades
						if (!String.IsNullOrEmpty(sInvoiceRef) && !String.IsNullOrEmpty(sSenderFiscalId))
						{
							// Crida al servei
							itmResponse = RequestInvoiceFiles(sSenderFiscalId, RequestInvoicesToDownload(
								sSenderFiscalId, sInvoiceRef, sUserName, sUserPassword), 3, sUserName
								, sUserPassword);
							if (itmResponse != null && itmResponse.StatusOK)
							{
								// Comprova si s'ha descarregat l'arxiu de la factura
								if (itmResponse.FicheroZipB64 != null)
								{
									// Genera l'arxiu de la factura amb les dades rebudes
									sInvoiceFilePath = ConfigurationManager.AppSettings["DldDirectoryPath"] +
										sSenderFiscalId + "_" + sInvoiceRef + ".zip";
									fsmInvoice = new FileStream(sInvoiceFilePath, FileMode.OpenOrCreate
										, FileAccess.Write);
									bFileData = Convert.FromBase64String(itmResponse.FicheroZipB64);
									fsmInvoice.Write(bFileData, 0, bFileData.GetUpperBound(0) + 1);
									fsmInvoice.Close();
									// Registra el resultat
									WriteLog("Invoice " + sInvoiceRef + " processed", sLogFilePath);
								}
								else
								{
									// Registra el resultat
									WriteLog("Invoice " + sInvoiceRef + " no file found", sLogFilePath);
								}
							}
							else
							{
								// Registra el resultat
								WriteLog("Invoice " + sInvoiceRef + " not processed", sLogFilePath);
								if (itmResponse != null)
								{
									WriteLog("Error " + itmResponse.ErrorMessage, sLogFilePath);
								}
								else
								{
									WriteLog("Error no response", sLogFilePath);
								}
							}
						}
						else
						{
							// Comprova si no s'ha pogut recollir la referencia de la factura
							if (String.IsNullOrEmpty(sInvoiceRef))
							{
								// Registra el resultat
								WriteLog("No invoice reference found", sLogFilePath);
							}
							else
							{
								// Registra el resultat
								WriteLog("No sender fiscal id found", sLogFilePath);
							}
						}
					}
				}
				else
				{
					// Registra el resultat per pantalla
					Console.WriteLine("WARN: Process()");
					Console.WriteLine("WARN: No rows found");
				}
				// Registra el resultat per pantalla
				Console.WriteLine("INFO: Process()");
				Console.WriteLine("INFO: Successful");
			}
			catch (Exception ectProcess)
			{
				// Registra l'error per pantalla
				Console.WriteLine("ERROR: Process()");
				Console.WriteLine("ERROR: " + ectProcess.Message);
			}
			finally
			{
				// Comprova si la connexió a l'arxiu encara és oberta
				if (smrCsvFile != null)
				{
					// Tanca la connexió a l'arxiu
					smrCsvFile.Close();
					smrCsvFile.Dispose();
				}
			}
		}

		private static WebService.RespuestaWsDescargarFacturas RequestInvoiceFiles(String prmSenderFiscalId
			, WebService.RespuestaWsConsultarFacturas prmInvoices, Int32 prmFileFormatId, String prmUserName
			, String prmUserPassword)
		{
			// Crea el contenidor de la connexió al servei web
			WebService.ServicesWebSoapClient itmWebService = null;
			// Crea el contenidor del resultat
			WebService.RespuestaWsDescargarFacturas rifResult = null;
			try
			{
				// Comprova si la sol·licitud de factures ha funcionat correctament
				if (prmInvoices != null && prmInvoices.StatusOK)
				{
					// Comprova si hi ha factures a descarregar
					if (prmInvoices.Facturas != null && prmInvoices.Facturas.Length > 0)
					{
						// Inicialitza el contenidor de la connexió al servei web
						itmWebService = new WebService.ServicesWebSoapClient();
						itmWebService.Endpoint.Address = new EndpointAddress(ConfigurationManager.AppSettings["SvcPath"]);
						// Sol·licita les factures a descarregar
						rifResult = itmWebService.DescargarFacturasFormato(prmSenderFiscalId
							, prmInvoices.Facturas, prmFileFormatId, prmUserName, prmUserPassword);
					}
					else
					{
						// Inicialitza el contenidor del resultat
						rifResult = new WebService.RespuestaWsDescargarFacturas();
						// Informa que la sol·licitud de factures no ha retornat factures a descarregar
						rifResult.StatusOK = prmInvoices.StatusOK;
					}
				}
				else
				{
					// Inicialitza el contenidor del resultat
					rifResult = new WebService.RespuestaWsDescargarFacturas();
					// Informa que la sol·licitud de factures no ha funcionat correctament
					rifResult.ErrorMessage = prmInvoices.ErrorMessage;
					rifResult.ErrorStackTrace = prmInvoices.ErrorStackTrace;
					rifResult.StatusOK = prmInvoices.StatusOK;
				}
			}
			catch (Exception ex)
			{
			}
			// Retorna el resultat
			return rifResult;
		}

		private static WebService.RespuestaWsConsultarFacturas RequestInvoicesToDownload(String prmSenderFiscalId
			, String prmInvoiceReference, String prmUserName, String prmUserPassword)
		{
			// Crea el contenidor dels paràmetres a enviar al servei web
			WebService.ParamWSConsultarEstadoFacturas wsvParameters = null;
			// Crea el contenidor de la connexió al servei web
			WebService.ServicesWebSoapClient itmWebService = null;
			// Crea el contenidor del resultat
			WebService.RespuestaWsConsultarFacturas ridResult = null;
			try
			{
				// Comprova si s'informa el CIF de l'emisor
				if (!String.IsNullOrEmpty(prmSenderFiscalId))
				{
					// Inicialitza el contenidor dels paràmetres a enviar al servei web
					wsvParameters = new WebService.ParamWSConsultarEstadoFacturas();
					// Recull els paràmetres a enviar al servei web
					wsvParameters.Estado = WebService.estadoFactura.noasignada;
					wsvParameters.IdFiscalEmpresaEmisora = prmSenderFiscalId;
					wsvParameters.IdFiscalEntidadUsuario = prmSenderFiscalId;
					wsvParameters.RefFactura = prmInvoiceReference;
					// Inicialitza el contenidor de la connexió al servei web
					itmWebService = new WebService.ServicesWebSoapClient();
					itmWebService.Endpoint.Address = new EndpointAddress(ConfigurationManager.AppSettings["SvcPath"]);
					// Sol·licita les factures a descarregar
					ridResult = itmWebService.ConsultarEstadoFacturasNoRestriccionDepartamentos(wsvParameters
						, prmUserName, prmUserPassword);
				}
			}
			catch (Exception ectInvoicesToDownload)
			{
				ridResult = new WebService.RespuestaWsConsultarFacturas();
				ridResult.StatusOK = false;
				ridResult.ErrorMessage = ectInvoicesToDownload.Message;
			}
			// Retorna el resultat
			return ridResult;
		}

		private static Boolean Validate()
		{
			// Crea el contenidor de la ruta al arxiu
			String sFilePath = String.Empty;
			// Crea el contenidor del resultat
			Boolean bResult = false;
			try
			{
				// Recull la ruta al arxiu
				sFilePath = ConfigurationManager.AppSettings["DtaFilePath"];
				// Comprova que l'arxiu amb les dades a processar existeix
				if (!String.IsNullOrEmpty(sFilePath) && File.Exists(sFilePath))
				{
					// Informa del resultat
					bResult = true;
				}
				else
				{
					// Informa del resultat
					bResult = false;
					// Registra el resultat per pantalla
					Console.WriteLine("WARN: Validate()");
					if (!String.IsNullOrEmpty(sFilePath))
					{
						// Informa que no s'ha informat la ruta a l'arxiu
						Console.WriteLine("WARN: File path not informed");
					}
					else
					{
						// Informa que no s'ha trobat l'arxiu
						Console.WriteLine("WARN: File not found");
					}
				}
			}
			catch (Exception ectValidate)
			{
				// Registra l'error per pantalla
				Console.WriteLine("ERROR: Validate()");
				Console.WriteLine("ERROR: " + ectValidate.Message);
			}
			// Comprova si s'ha validat correctament
			if (bResult)
			{
				// Registra el resultat per pantalla
				Console.WriteLine("INFO: Validate()");
				Console.WriteLine("INFO: Successful");
			}
			// Retorna el resultat
			return bResult;
		}

		/// <summary>
		/// Escriu un missatge a un arxiu de registre
		/// </summary>
		/// <param name="prmMissatge">Un System.String amb el missatge</param>
		/// <param name="prmRutaArxiu">Un System.String amb la ruta a l'arxiu de registre</param>
		public static void WriteLog(String prmMissatge, String prmRutaArxiu)
		{
			// Crea el contenidor del missatge
			String sMessage = String.Empty;
			// Crea el contenidor de la ruta d'emmagatzemament de registres
			String sRutaRegistre = String.Empty;
			// Crea el contenidor de l'escriptor de text
			StreamWriter stwArxiu = null;
			try
			{
				// Afegeix la data i el nivell d'importància al missatge
				sMessage = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\t";
				// Afegeix el missatge rebut
				sMessage += prmMissatge;
				// Comprova si la ruta a l'arxiu de registre no ve informada
				if (String.IsNullOrEmpty(prmRutaArxiu))
				{
					// Inicialitza el contenidor de la ruta d'emmagatzemament de registres
					prmRutaArxiu = String.Empty;
				}
				// Genera la ruta a l'arxiu de registre
				sRutaRegistre = String.Concat(prmRutaArxiu, DateTime.Today.ToString("yyyy-MM-dd"), ".rgt");
				// Comprova si l'arxiu existeix
				if (File.Exists(sRutaRegistre))
				{
					// Obre l'arxiu i el prepara per afegir text
					stwArxiu = System.IO.File.AppendText(sRutaRegistre);
				}
				else
				{
					// Crea l'arxiu i el prepara per escriure text
					stwArxiu = System.IO.File.CreateText(sRutaRegistre);
				}
				// Comprova si l'arxiu de registre s'ha obert
                if (stwArxiu != null)
                {
                    // Posa el text a escriure en cua
					stwArxiu.WriteLine(sMessage);
                    // Escriu el text a l'arxiu
                    stwArxiu.Flush();
                }
				else
				{
					// Registra el resultat per pantalla
					Console.WriteLine("WARN: WriteLog()");
					Console.WriteLine("WARN: No log file found");
				}
            }
            catch (Exception ectLog)
            {
				// Registra l'error per pantalla
				Console.WriteLine("ERROR: WriteLog()");
				Console.WriteLine("ERROR: " + ectLog.Message);
			}
            finally
            {
                // Tanca l'accés a l'arxiu
                if (stwArxiu != null) stwArxiu.Close();
            }
		}

		#endregion Privats

		#endregion Mètodes

	}

}
